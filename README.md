# SmartCityWasteManagement




Benvenuto nella repository GitLab del progetto SmartCityWasteManagement.




## Introduzione



Il sistema è composto da 9 microservizi, che compongono la parte backend del sistema, e da un progetto Angular, che ne compone la parte frontend, insieme ad un'applicazione node.js.

La repository è composta da 12 branch, ciascuno contente il codice delle singole componenti del sistema (9 microservizi + 1 frontend angular + 1 app node.js + 1 main, che contiene questo file Readme).




## Backend



La parte backend del sistema è suddivisa ciascuna in 9 microservizi sviluppati con Java SpringBoot, insieme al tool Gradle.

1) Autenticazione
2) Cassonetti
3) Rifiuti
4) Conferimenti (subscriber RabbitMQ)
5) Percorsi
6) Indici Performance
7) Pagamenti
8) Allarmi (include un broker RabbitMQ) (publisher RabbitMQ)
9) Email (subscriber RabbitMQ)

Tutti i servizi espongono API REST, e il servizio degli allarmi ospita il broker RabbitMQ usato dallo stesso e da altri servizi, oltre all'app node.js.



### 1) Autenticazione


Il servizio di autenticazione implementa Spring Security e si occupa, da un lato di garantire la corretta autenticazione di tutte le richieste del sistema, attraverso l'uso di token jwt opportunamente generati. Dall'altro, gestisce il database degli utenti, suddivisi nei seguenti ruoli: amministratore di azienda, amministratore comunale e cliente (cittadino). 

Nella prima fase di login, l'utente inserisce le sue credenziali (username e password) e il servizio, se le credenziali sono corrette, genera un token jwt (con validità di 10 ore) associato al suo username, che viene utilizzato per le richieste successive.

Al fine di garantire una corretta autorizzazione e identificazione dei ruoli degli utenti, quasi tutti i metodi dei microservizi richiedono autenticazione tramite token jwt. Quando un servizio riceve una richiesta, verifica se il jwt è presente nell'intestazione. Nel caso affermativo, per ottenere il ruolo dell'utente proprietario del token, esso invia una richiesta (con il jwt nell'intestazione) al servizio di autenticazione. Quest'ultimo lo decodifica attraverso una chiave segreta, risale all'username dell'utente e restituisce il ruolo. In questo modo, il servizio può autorizzare o meno l'accesso al metodo.



### 2) Cassonetti


Questo servizio si occupa della gestione della base di dati dei cassonetti. Implementa, oltre al ciclo CRUD, i metodi di svuotamento e riempimento dei cassonetti. Quando il cassonetto si riempie, esso invia una richiesta (senza autenticazione), contenente l'identificativo del cassonetto al servizio degli allarmi, che genera un allarme di sovrabbondanza e notifica l'amministratore di azienda dell'evento inviando una mail.



### 3) Rifiuti


Il servizio dei rifiuti gestisce il database dei rifiuti, e implementa i metodi CRUD.



### 4) Conferimenti


Il servizio dei conferimenti si occupa della corretta gestione del database dei conferimenti, ed implementa, fra tutti, il metodo di creazione del conferimento.
Il servizio è in ascolto alla coda 'getta_rifiuto' del broker RabbitMQ. Alla ricezione di un messaggio (inviato dall'applicazione node.js), che include l'identificativo del cliente (incluso il token jwt), quello del rifiuto e del cassonetto, oltre ai rispettivi tipi (e volume per il rifiuto) viene richiamato il metodo di creazione del conferimento. In questo metodo, viene inoltre richiamato quello relativo al rimepimento del cassonetto.



### 5) Percorsi


Il servizio dei percorsi gestisce la base di dati dei percorsi di pulizia dei cassonetti. Un percorso è associato ad uno o più cassonetti e, quando esso viene creato, richiama il metodo di svuotamento per ogni cassonetto incluso.



### 6) Indici Performance


Questo servizio gestisce il database degli indici di performance per ogni cliente. Gli indici performance vengono creati su base mensile, e hanno un valore percentuale che viene calcolato dividendo il numero dei conferimenti corretti (tipo cassonetto = tipo rifiuto) per quello dei conferimenti sbagliati (tipo cassonetto =/= tipo rifiuto) e moltiplicando tutto per 100.
Vengono implementati diversi metodi tra cui, oltre a quello già citato della creazione, metodi che restituiscono medie mensili e annuali per i singoli clienti o per l'intero comune.



### 7) Pagamenti


Il servizio dei pagamenti si occupa della gestione della base di dati dei pagamenti. I pagamenti vengono generati su base annuale: 
a partire da un importo di base, 100€ viene aggiunta o sottratta una somma in base al valore di performance annuale per ogni cliente.
Se la media annuale è inferiore al 90%, l'importo finale sarà uguale a (importo base - (media annuale) - 100). Altrimenti, se la media è maggiore o uguale a 90%, l'importo sarà (importo base - ((media annuale - 90) * 2)). Ad esempio, se un cliente ha un valore di performance annuale pari al 70%, l'importo da pagare per lui sarà pari a 130€. Al contrario, se il valore di performance annuale per un cliente è del 94%, egli dovrà pagare 92€.
Se un pagamento risulta ancora non pagato oltre un anno dalla data di emissione, il cliente può essere disabilitato, e potrà essere riabilitato soltanto dopo aver pagato gli arretrati.



### 8) Allarmi


Il servizio degli allarmi si occupa della creazione e gestione del database dedicato agli allarmi di sovrabbondanza. L'allarme contiene l'identificativo del cassonetto che l'ha generato e la data di generazione. Una volta generato l'allarme, il servizio invia un messaggio alla coda 'coda_allarme' del broker RabbitMQ.



### 9) Email


Il servizio email si occupa dell'invio di email di sensibilizzazione ai clienti con un indice di performance basso, e dell'invio di email all'amministratore di azienda con il fine di notificare nuovi allarmi di sovrabbondanza per i cassonetti. A tal proposito, infatti, il servizio è sottoscritto alla coda 'coda_allarme' del broker RabbitMQ: alla ricezione di un nuovo messaggio, viene richiamato il metodo preposto all'invio dell'email all'amministratore.




## Frontend



La parte di frontend del sistema è composta da una Web-Application sviluppata in Angular, che fornisce l'interfaccia utente per i tre ruoli dell'utente, e da un'applicazione node.js, volta a simulare la vicinanza "fisica" del cassonetto al cliente quando getta un rifiuto.



### Angular Web-App


L'interfaccia utente comincia con la schermata iniziale di login, dove l'utente, per autenticarsi, deve inserire username e password. Qui è inoltre possibile aprire la pagina per registrare un nuovo cliente.
In base al ruolo dell'utente che si autentica, viene indirizzato alla rispettiva schermata iniziale.

Nella schermata iniziale, il cliente può scegliere tra diverse operazioni: 
 - effettuare conferimenti; 
 - visualizzare lo storico dei  pagamenti (con la possibilità di pagare quelli non ancora pagati);
 - visualizzare lo storico degli indici di performance;
 - visualizzare lo storico dei conferimenti.
 - modificare password, Indirizzo, telefono e indirizzo email.

L'amministratore comunale può:
 - visualizzare lo storico dei pagamenti del sistema, creare nuovi pagamenti, disabilitare e riabilitare i clienti;
 - visualizzare lo storico degli indici di performance del comune, generare quelli per il mese precedente, prendere visione, su
   base mensile, dello storico degli indici dei clienti, con la possibilità di inviare un avviso di sensibilizzazione via email a
   quelli con un valore di performance basso;
 - visualizzare lo storico dei conferimenti di tutti i cittadini.

L'amministratore aziendale può:
 - visualizzare su mappa lo stato dei cassonetti;
 - avviare dei percorsi di pulizia per i cassonetti che sono oltre una certa soglia di riempimento, e visualizzarne lo storico;
 - creare, modificare ed eliminare i cassonetti;
 - creare, modificare ed eliminare i rifiuti;
 - visualizzare lo storico degli allarmi di sovrabbondanza.



 ### App node.js


L'applicazione node.js simula la presenza fisica del cassonetto: quando un cittadino getta un rifiuto, la Web-App di Angular invia a tale applicazione i dettagli del conferimento (dettagli del cassonetto, del rifiuto, id del cliente e relativo token jwt) che, a sua volta, inoltra pubblicando un messaggio alla coda 'getta_rifiuto' del broker RabbitMQ citato prima. A questo punto, il servizio dei conferimenti crea il nuovo conferimento, e invia la richiesta al servizio dei cassonetti di aggiornare il riempimento di quello associato.
